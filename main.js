const toTop = document.querySelector(".to-top");

window.addEventListener("scroll", () => {
  if (window.pageYOffset > 100) {
    toTop.classList.add("active");
  } else {
    toTop.classList.remove("active");
  }
})


function Search() {
  var x = document.getElementById("text-search");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else if (x.style.display === "block"){
    x.style.display = "none";
  }
  else {
    x.style.display = "none";
  }
}